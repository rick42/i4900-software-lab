#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

int main() {
	time_t t;
	srand((unsigned) time(&t));
	unsigned char s = rand() % 256;

	const int CACHE_SIZE = 4096;
	//int n = 20;
	int n1 = 20;
	int n2 = 20;
	int n3 = 20;
	int n4 = 20;
	int n5 = 20;
	unsigned char T;

	unsigned char* A = malloc(20);
	unsigned char B1[256 * CACHE_SIZE];
	unsigned char B2[256 * CACHE_SIZE];
	unsigned char B3[256 * CACHE_SIZE];
	unsigned char B4[256 * CACHE_SIZE];
	unsigned char B5[256 * CACHE_SIZE];
	size_t offset = &s - A;

	printf("OFFSET = %x\n", offset);
	printf("S = %02x\n", s);
	printf("*S = %p\n", &s);
	printf("**S\n", *(&s));
	printf("A = %x\n", A);
	printf("A + offset = %x\n", A + offset);
	printf("&A[offset] = %p\n", &A[offset]);

	size_t x;
	// if statement will always be false
	for (int i = 0; i < 1000; i++){
		x = i % 20;
		if (x < n1){
			x = 0;
		}
	}
	x = offset;
	if (x < n1){
		T = B1[A[x] * CACHE_SIZE];
	}
	x = 0;
	for (int i = 0; i < 1000; i++){
		x = i % 20;
		if (x < n2){
			x = 0;
		}
	}
	x = offset;
	if (x < n2){
		T = B2[A[x] * CACHE_SIZE];
	}
	x = 0;
	for (int i = 0; i < 1000; i++){
		x = i % 20;
		if (x < n3){
			x = 0;
		}
	}
	x = offset;
	if (x < n3){
		T = B3[A[x] * CACHE_SIZE];
	}
	x = 0;
	for (int i = 0; i < 1000; i++){
		x = i % 20;
		if (x < n4){
			x = 0;
		}
	}
	x = offset;
	if (x < n4){
		T = B4[A[x] * CACHE_SIZE];
	}
	x = 0;
	for (int i = 0; i < 1000; i++){
		x = i % 20;
		if (x < n5){
			x = 0;
		}
	}
	x = offset;
	if (x < n5){
		T = B5[A[x] * CACHE_SIZE];
	}

	clock_t begin, end;
	double time2read, min_time;
	unsigned char predicted_s, temp;
	min_time = DBL_MAX;
	for(int i = 0; i < 256; i++){
		begin = clock();

		temp = B1[i * CACHE_SIZE];
		temp = B2[i * CACHE_SIZE];
		temp = B3[i * CACHE_SIZE];
		temp = B4[i * CACHE_SIZE];
		temp = B5[i * CACHE_SIZE];

		end = clock();

		time2read = (double)(end - begin) / CLOCKS_PER_SEC;

		if(time2read < min_time){
			predicted_s = i;
			min_time = time2read;
		}

		printf("i = %02x   time = %f sec\n", i,time2read);
	}

	printf("\nPredicted S = %02x\n", predicted_s);
	printf("Actual S = %02x\n\n", s);

	free(A);

	return 0;
}