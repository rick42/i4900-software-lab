#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

int main() {
	time_t t;
	srand((unsigned) time(&t));

	const int CACHE_SIZE = 4096;
	unsigned char s;
	unsigned char A1[256 * CACHE_SIZE];
	unsigned char A2[256 * CACHE_SIZE];
	unsigned char A3[256 * CACHE_SIZE];
	unsigned char A4[256 * CACHE_SIZE];
	unsigned char A5[256 * CACHE_SIZE];

	s = rand() % 256;
	A1[s * CACHE_SIZE] = 1;
	A2[s * CACHE_SIZE] = 2;
	A3[s * CACHE_SIZE] = 3;
	A4[s * CACHE_SIZE] = 4;
	A5[s * CACHE_SIZE] = 5;

	clock_t begin, end;
	double time2read, min_time;
	unsigned char predicted_s, temp;
	min_time = DBL_MAX;
	for(int i = 0; i < 256; i++){
		begin = clock();

		temp = A1[i * CACHE_SIZE];
		temp = A2[i * CACHE_SIZE];
		temp = A3[i * CACHE_SIZE];
		temp = A4[i * CACHE_SIZE];
		temp = A5[i * CACHE_SIZE];

		end = clock();

		time2read = (double)(end - begin) / CLOCKS_PER_SEC;

		if(time2read < min_time){
			predicted_s = i;
			min_time = time2read;
		}

		printf("i = %02x   time = %f sec\n", i,time2read);
	}

	printf("\nPredicted S = %02x\n", predicted_s);
	printf("Acual S = %02x\n\n", s);

	return 0;
}